<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Seguros $model */

$this->title = 'Número de poliza: ' . $model->numero_poliza;
$this->params['breadcrumbs'][] = ['label' => 'Seguros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alinear">
    <div class="seguros-view">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <p>
            <?= Html::a('Actualizar', ['update', 'numero_poliza' => $model->numero_poliza], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'numero_poliza' => $model->numero_poliza], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro de que quieres eliminar este registro?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'matricula',
                [
                    'label' => 'Marca',
                    'value' => function ($model) {
                        return $model->matricula0->marca;
                    }
                ],
                [
                    'label' => 'Modelo',
                    'value' => function ($model) {
                        return $model->matricula0->modelo;
                    }
                ],
                'numero_poliza',
                [
                    'attribute' => 'precio',
                    'value' => function ($model) {
                        return strpos($model->precio, '.') !== false 
                            ? number_format($model->precio, 2, ',', '.') 
                            : number_format($model->precio, 0, ',', '.');
                    },
                    'format' => 'raw',
                ], 
            ],
        ]) ?>

    </div>
</div>