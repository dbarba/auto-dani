<?php

use app\models\Seguros;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Seguros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seguros-index">

    <h1 class="tituloTablas"><?= Html::encode($this->title) ?></h1>

    <div class="alinear-btn">
        <div class="btn-mantenimiento">
            <?= Html::a('Nuevo Seguro', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'matricula',
            [
                'label' => 'Marca',
                'value' => function ($model) {
                    return $model->matricula0->marca;
                }
            ],
            [
                'label' => 'Modelo',
                'value' => function ($model) {
                    return $model->matricula0->modelo;
                }
            ],
            'numero_poliza',
            [
                'attribute' => 'precio',
                'value' => function ($model) {
                    return strpos($model->precio, '.') !== false 
                        ? number_format($model->precio, 2, ',', '.') 
                        : number_format($model->precio, 0, ',', '.');
                },
                'format' => 'raw',
            ], 
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Seguros $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numero_poliza' => $model->numero_poliza]);
                 }
            ],
        ],
    ]); ?>


</div>
