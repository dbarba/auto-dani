<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Coches;
use app\models\Seguros;

/** @var yii\web\View $this */
/** @var app\models\Seguros $model */

$this->title = 'Nuevo Seguro';
$this->params['breadcrumbs'][] = ['label' => 'Seguros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alinear">
    <div class="seguros-create">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero_poliza')->textInput(['placeholder' => 'Introduce un número de póliza con al menos 6 dígitos', 'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57', 'autocomplete' => 'off']) ?>

        <?= $form->field($model, 'precio')->textInput(['onchange' => 'replaceCommaWithDot(this)', 'autocomplete' => 'off']) ?>

        <?= $form->field($model, 'matricula')->dropDownList(
           \yii\helpers\ArrayHelper::map(
                Coches::find()
                    ->where(['not in', 'matricula', Seguros::find()->select('matricula')->column()])
                    ->all(),
                'matricula',
                function ($coche) {
                    return $coche->matricula . ' - ' . $coche->marca . ' ' . $coche->modelo;
                }
            ),
            ['prompt' => 'Selecciona una matrícula', 'autocomplete' => 'off']
        ) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>