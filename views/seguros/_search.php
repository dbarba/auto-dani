<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="matricula-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'matricula')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca la matrícula a buscar', 'style' => 'width: 260px;', 'autocomplete' => 'off'])->label(false) ?>
            <?php if ($model->hasErrors('matricula')): ?>
                <div class="alert alert-danger" style="margin-top: 5px;">
                    <?= $model->getFirstError('matricula') ?>
                </div>
            <?php endif; ?>
        </div>
    
        <div class="form-group" style="margin-left: 23px; margin-right: -29px;">
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-secondary btn-buscador']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

