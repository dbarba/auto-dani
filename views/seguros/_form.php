<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Seguros $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="seguros-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero_poliza')->textInput(['placeholder' => 'Introduce un número de póliza con al menos 6 dígitos', 'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'precio')->textInput(['onchange' => 'replaceCommaWithDot(this)', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'matricula')->dropDownList(
            \yii\helpers\ArrayHelper::map(
                \app\models\Coches::find()->all(),
                'matricula',
                function ($coche) {
                    return $coche->matricula . ' - ' . $coche->marca . ' ' . $coche->modelo;
                }
            ),
            ['prompt' => 'Selecciona una matrícula', 'autocomplete' => 'off']
        ) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
