<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Atienden $model */

$this->title = 'Nuevo Registro de Atención';
$this->params['breadcrumbs'][] = ['label' => 'Atienden', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alinear">
    <div class="atienden-create">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>