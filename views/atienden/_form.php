<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\Atienden $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="atienden-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_empleado')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Empleados::find()->all(),
            'id',
            function ($empleado) {
                    return $empleado->nombre_completo;
                }
        ),
        ['prompt' => 'Selecciona un empleado', 'autocomplete' => 'off']
    )->label('Empleado') ?>

    <?= $form->field($model, 'dni_cliente')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Clientes::find()->all(),
            'dni',
            function ($cliente) {
                    return $cliente->dni . ' - ' . $cliente->nombre_completo;
                }
        ),
        ['prompt' => 'Selecciona un cliente', 'autocomplete' => 'off']
    )->label('Cliente') ?>

    <?= $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'readonly' => true],
        'clientOptions' => [
            'minDate' => '-6m', //Esto establece la mínima fecha seleccionable como tres meses antes de la fecha actual
            'maxDate' => 0, //Esto establece la máxima fecha seleccionable como la fecha actual
            'changeMonth' => true, //Permite la selección directa del mes
            'changeYear' => true, //Permite la selección directa del año
            'yearRange' => '+0:+0', //Rango de años permitidos
            'theme' => 'base', 
            'placeholder' => 'Seleccione fecha',
            'autocomplete' => 'off',
            'beforeShowDay' => new \yii\web\JsExpression('
                function(date) {
                    var day = date.getDay();
                    return [(day != 0), ""];
                }
            ')
        ],
    ])->label('Fecha de atención') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
