<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Atienden $model */

$this->title ='Registro de atención individual';
$this->params['breadcrumbs'][] = ['label' => 'Atienden', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alinear">
    <div class="atienden-view">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <p>
            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro de que quieres eliminar este registro?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'empleado.nombre_completo',
                    'label' => Yii::t('app', 'Empleado'),
                ],
                [
                    'label' => 'Cliente',
                    'value' => function ($model) {
                        return $model->dniCliente->nombre_completo;
                    }
                ],
                'dni_cliente',  

                [
                    'attribute' => 'fecha',
                    'format' => ['date', 'php:d-m-Y'],
                ],
            ],
        ]) ?>

    </div>
</div>