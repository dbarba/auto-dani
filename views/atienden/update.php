<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Atienden $model */

$this->title = 'Actualizar Registro de Atención';
$this->params['breadcrumbs'][] = ['label' => 'Atienden', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="atienden-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>