<?php

use app\models\Atienden;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Registro de atención';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atienden-index">

    <h1 class="tituloTablas">Registro de atención</h1>

    <div class="alinear-btn">
        <div class="btn-mantenimiento">
            <?= Html::a('Nuevo Registro', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'empleado.nombre_completo',
                'label' => Yii::t('app', 'Empleado'),
            ],
            [
                'label' => 'Cliente',
                'value' => function ($model) {
                    return $model->dniCliente->nombre_completo;
                }
            ],
            'dni_cliente',  
            
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d-m-Y'],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Atienden $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

</div>
