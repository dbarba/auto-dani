<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Mantenimientos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="mantenimientos-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Coches::find()->where(['estado' => 0])->all(),
            'matricula',
            function ($coche) {
                    return $coche->matricula . ' - ' . $coche->marca . ' ' . $coche->modelo;
                }
        ),
        ['prompt' => 'Selecciona una matrícula', 'autocomplete' => 'off']
    ) ?>

    <?= $form->field($model, 'descripcion')->textArea(['rows' => 4, 'autocomplete' => 'off'])->label('Descripción') ?>

    <?= $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'readonly' => true],
        'clientOptions' => [
            'minDate' => '-6m', //Esto establece la mínima fecha seleccionable como tres meses antes de la fecha actual
            'maxDate' => 0, //Esto establece la máxima fecha seleccionable como la fecha actual
            'changeMonth' => true, //Permite la selección directa del mes
            'changeYear' => true, //Permite la selección directa del año
            'yearRange' => '+0:+0', //Rango de años permitidos
            'theme' => 'base', 
            'placeholder' => 'Seleccione fecha',
            'autocomplete' => 'off',
            'beforeShowDay' => new \yii\web\JsExpression('
                function(date) {
                    var day = date.getDay();
                    return [(day != 0), ""];
                }
            ')
        ],
    ])->label('Fecha') ?>
    
    <?= $form->field($model, 'kilometros')->textInput(['onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57', 'autocomplete' => 'off'])->label('Kilometros') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

