<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Mantenimientos $model */

$this->title = 'Mantenimiento: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Mantenimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] =  $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alinear">
    <div class="mantenimientos-view">

        <h1> <?= Html::encode($this->title) ?></h1>
        <br>
        <p>
            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro de que quieres eliminar este registro?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'matricula',
                [
                    'label' => 'Marca',
                    'value' => function ($model) {
                        return $model->matricula0->marca;
                    }
                ],
                [
                    'label' => 'Modelo',
                    'value' => function ($model) {
                        return $model->matricula0->modelo;
                    }
                ],
                'descripcion',
                [
                    'attribute' => 'kilometros',
                    'value' => function ($model) {
                        return number_format($model->kilometros, 0, ',', '.');
                    },
                ],
                [
                    'attribute' => 'fecha',
                    'format' => ['date', 'php:d-m-Y'], // Aquí se especifica el formato de la fecha
                ],
                
            ],
        ]) ?>

    </div>
</div>