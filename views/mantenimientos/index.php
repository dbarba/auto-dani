<?php

use app\models\Mantenimientos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Mantenimientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mantenimientos-index">

    <h1 class="tituloTablas"><?= Html::encode($this->title) ?></h1>

    <div class="alinear-btn">
        <div class="btn-mantenimiento">
            <?= Html::a('Nuevo Mantenimiento', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//      'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'matricula',
            [
                'attribute' => 'descripcion',
                'headerOptions' => ['style' => 'width: 650px;'],
            ],
            
            [
                'attribute' => 'kilometros',
                'value' => function ($model) {
                    return number_format($model->kilometros, 0, ',', '.');
                },
            ],
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d-m-Y'],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Mantenimientos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
