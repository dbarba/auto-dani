<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mantenimientos $model */

$this->title = 'Actualizar Mantenimiento';
$this->params['breadcrumbs'][] = ['label' => 'Mantenimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="mantenimientos-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>