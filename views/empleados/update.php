<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Empleados $model */

$this->title = 'Actualizar Empleado';
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="empleados-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>