<?php

use app\models\Empleados;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-index">

    <h1 class="tituloTablas"><?= Html::encode($this->title) ?></h1>

    <div class="alinear-btn">
        <div class="btn-mantenimiento">
            <?= Html::a('Nuevo Empleado', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'nombre_completo',
                'headerOptions' => ['style' => 'width: 1070px;'],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Empleados $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
