<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = 'Actualizar Cliente: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="telefonos-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
            'telefonos' => $telefonos,
        ]) ?>

    </div>
</div>