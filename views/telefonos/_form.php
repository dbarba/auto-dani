<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="telefonos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'readonly' => true, 'autocomplete' => 'off'])->label('Cliente') ?>

    <?php foreach ($telefonos as $index => $telefono): ?>
        <?= $form->field($telefono, "[$index]telefono")->textInput(['maxlength' => true, 'autocomplete' => 'off'])->label("Teléfono " . ($index + 1)) ?>
    <?php endforeach; ?>

    <?php for ($i = count($telefonos); $i < 2; $i++): ?>
        <?php $nuevoTelefono = new \app\models\Telefonos(); ?>
        <?= $form->field($nuevoTelefono, "[$i]telefono")->textInput(['maxlength' => true, 'autocomplete' => 'off'])->label("Teléfono " . ($i + 1)) ?>
    <?php endfor; ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>




