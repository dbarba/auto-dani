<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = 'Nuevo Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alinear">
    <div class="telefonos-create">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'dni_cliente')->textInput(['maxlength' => true, 'readonly' => true, 'autocomplete' => 'off'])->label('Cliente') ?>

        <?= $form->field($model, 'telefono[]')->textInput(['maxlength' => true, 'autocomplete' => 'off'])->label('Teléfono 1') ?>

        <?= $form->field($model, 'telefono[]')->textInput(['maxlength' => true, 'autocomplete' => 'off'])->label('Teléfono 2') ?>

        

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
