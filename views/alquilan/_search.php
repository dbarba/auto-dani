<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="matricula-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row contenedor-registro">
        <div class="col-md-8 contenedor-buscador">
            <?= $form->field($model, 'dni_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el DNI del cliente a buscar', 'style' => 'width: 293px;', 'autocomplete' => 'off'])->label(false) ?>
            <?php if ($model->hasErrors('dni_cliente')): ?>
                <div class="alert alert-danger" style="margin-top: 5px;">
                    <?= $model->getFirstError('dni_cliente') ?>
                </div>
            <?php endif; ?>
        </div>
    
        <div class="form-group" style="margin-left: 53px; margin-right: -22px;">
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-secondary btn-buscador']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

