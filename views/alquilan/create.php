<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Coches;
use app\models\Seguros;

/** @var yii\web\View $this */
/** @var app\models\Alquilan $model */
/** @var array $options */ // Agregar esta línea
/** @var array $precios */ // Agregar esta línea

$this->title = 'Nuevo Contrato';
$this->params['breadcrumbs'][] = ['label' => 'Alquilan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alinear">
    <div class="alquilan-create">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>

        <?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'dni_cliente')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Clientes::find()->all(),
            'dni',
            function ($cliente) {
                return $cliente->dni . ' - ' . $cliente->nombre_completo;
            }
        ),
        ['prompt' => 'Selecciona un cliente', 'autocomplete' => 'off']
        )->label('Cliente') ?>
    
        <?= $form->field($model, 'matricula')->dropDownList(ArrayHelper::map($options, 'data-precio', 'label'), [
            'prompt' => 'Seleccione un coche',
            'id' => 'select-matricula', // Agregar un ID al campo select
            'autocomplete' => 'off'
        ])->label('Matrícula del coche') ?>
        
        <!--Campo oculto para la matricula-->
        <?= Html::hiddenInput('Alquilan[matricula]', '', ['id' => 'hidden-matricula-field']) ?>

        <?= $form->field($model, 'fecha_inicio')->textInput(['value' => date('d-m-Y'), 'readonly' => true])->label('Fecha de inicio') ?>

        <?= $form->field($model, 'fecha_fin')->widget(\yii\jui\DatePicker::classname(), [
            'dateFormat' => 'dd-MM-yyyy',
            'options' => ['class' => 'form-control', 'readonly' => true],
            'clientOptions' => [
                'minDate' => 0, //Esto establece la mínima fecha seleccionable como la fecha actual
                'maxDate' => '+3m', //Esto establece la máxima fecha seleccionable como tres meses desde la fecha actual
                'changeMonth' => true, //Permite la selección directa del mes
                'changeYear' => true, //Permite la selección directa del año
                'yearRange' => '-100:+2', //Rango de años permitidos
                'theme' => 'base',
                'placeholder' => 'Seleccione fecha',
                'autocomplete' => 'off',
                'beforeShowDay' => new \yii\web\JsExpression('
                    function(date) {
                        var day = date.getDay();
                        return [(day != 0), ""];
                    }
                ')
            ],
        ])->label('Fecha de fin') ?>
        
        <?= $form->field($model, 'precio_total')->textInput(['id' => 'precio-total', 'readonly' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <!-- Finaliza el formulario -->
        <?php ActiveForm::end(); ?>

    </div>
</div>

