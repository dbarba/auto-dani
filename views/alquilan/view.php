<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\mpdf\Pdf;

/** @var yii\web\View $this */
/** @var app\models\Alquilan $model */

$this->title = 'Contrato';
$this->params['breadcrumbs'][] = ['label' => 'Alquilan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alinear">
    <div class="alquilan-view">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="contenedor">
            <div class="contenedor1-contratos">
                <?= //Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) 
                 Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estás seguro de que quieres eliminar este registro?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <div class="contenedor2-contratos">
                <?= Html::a('Descargar contrato', ['alquilan/descargar','id' => $model->id], [
                    'class' => 'btn btn-info', 
                    'target'=>'_blank', 
                    'data-toggle'=>'tooltip', 
                    'title'=>'Descarga el contrato en PDF'
                ]) ?>
            </div>
        </div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'dni_cliente',
                [
                    'label' => 'Cliente',
                    'value' => function ($model) {
                        return $model->dniCliente->nombre_completo;
                    }
                ],
                'matricula',
                [
                    'label' => 'Marca',
                    'value' => function ($model) {
                        return $model->matricula0->marca;
                    }
                ],
                [
                    'label' => 'Modelo',
                    'value' => function ($model) {
                        return $model->matricula0->modelo;
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'format' => ['date', 'php:d-m-Y'],
                ],
                [
                    'attribute' => 'fecha_fin',
                    'format' => ['date', 'php:d-m-Y'],
                ],              
                [
                    'attribute' => 'precio_total',
                    'value' => function ($model) {
                        return strpos($model->precio_total, '.') !== false 
                            ? number_format($model->precio_total, 2, ',', '.') 
                            : number_format($model->precio_total, 0, ',', '.');
                    },
                    'format' => 'raw',
                ],
                
            ],
        ]) ?>

    </div>
</div>