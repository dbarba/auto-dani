<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Alquilan $model */
/** @var array $options */ // Agregar esta línea
/** @var array $precios */ // Agregar esta línea

?>

<div class="alinear">
    <div class="alquilan-create">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>

        <!-- Utiliza ActiveForm para el formulario -->
        <?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'dni_cliente')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Clientes::find()->all(),
            'dni',
            function ($cliente) {
                return $cliente->dni . ' - ' . $cliente->nombre_completo;
            }
        ),
        ['prompt' => 'Selecciona un cliente']
        )->label('Cliente') ?>

        <!-- Agrega un campo de menú desplegable para la matrícula -->
        
        <?= $form->field($model, 'matricula')->dropDownList(ArrayHelper::map($options, 'data-precio', 'label'), [
            'prompt' => 'Seleccione un coche',
            'id' => 'select-matricula', // Agregar un ID al campo select
        ])->label('Matrícula del coche') ?>

        <!-- Agrega un campo para la fecha de inicio -->
        <?= $form->field($model, 'fecha_inicio')->textInput(['value' => date('d-m-Y'), 'readonly' => true])->label('Fecha de inicio') ?>

        <!-- Agrega un campo para la fecha de fin -->
        <?= $form->field($model, 'fecha_fin')->widget(\yii\jui\DatePicker::className(), [
            'dateFormat' => 'dd-MM-yyyy',
            'options' => ['class' => 'form-control', 'readonly' => true],
            'clientOptions' => [
                'minDate' => 0, // Esto establece la mínima fecha seleccionable como la fecha actual
                'changeMonth' => true, // Permite la selección directa del mes
                'changeYear' => true, // Permite la selección directa del año
                'yearRange' => '-100:+2', // Rango de años permitidos
                'theme' => 'base',
                'placeholder' => 'Seleccione fecha',
                'beforeShowDay' => new \yii\web\JsExpression('
                    function(date) {
                        var day = date.getDay();
                        return [(day != 0), ""];
                    }
                ')
            ],
        ])->label('Fecha de fin') ?>

        
        <?= $form->field($model, 'precio_total')->textInput(['id' => 'precio-total', 'readonly' => true]) ?>

        <!-- Agrega un botón para enviar el formulario -->
        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <!-- Finaliza el formulario -->
        <?php ActiveForm::end(); ?>

    </div>
</div>


