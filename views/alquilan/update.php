<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Alquilan $model */

$this->title = 'Actualizar Contrato';
$this->params['breadcrumbs'][] = ['label' => 'Alquilan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="alquilan-update">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
