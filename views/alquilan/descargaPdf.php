<?php
use yii\helpers\Html;

//Asegurarse que el modelo está definido
if ($model !== null) :
    
    $meses = array(
    '01' => 'enero',
    '02' => 'febrero',
    '03' => 'marzo',
    '04' => 'abril',
    '05' => 'mayo',
    '06' => 'junio',
    '07' => 'julio',
    '08' => 'agosto',
    '09' => 'septiembre',
    '10' => 'octubre',
    '11' => 'noviembre',
    '12' => 'diciembre'
    );

    $numeroMes = date('m', strtotime($model->fecha_inicio));
    $nombreMes = $meses[$numeroMes];

?>

<?= Html::img('@web/img/logotipo.png', ['id' => 'logoPDF', 'style' => 'float: right; width: 300px; margin: 12px;']) ?>

<div class="pdf-cabecera" style="margin-right: 40px; text-align: center; color: #858585">
    <br>Polígono Industrial de Candina, Nave 4
    <br>Santander, Cantabria
    <br>De lunes a sábado de 9:00 a 14:00
    <br>Teléfono 601472066 / 942710418
</div>

<h2 class="titulo" style="text-align: center;">Contrato de alquiler</h2>
<br>

<p>
    <strong>Arrendador:</strong> autoDani S.L
    <br><strong>Dirección:</strong> Polígono Industrial de Candina, Nave 4. Santander, Cantabria
    <br><strong>Teléfono:</strong> 601472066 / 942710418
    <br><strong>Correo Electrónico:</strong> autodani@gmail.com
    <br>
    <br><strong>Arrendatario:</strong> <?= $cliente->nombre_completo?>
    <br><strong>DNI:</strong> <?= $model->dni_cliente ?>
    <br><strong>Dirección:</strong> <?= $cliente->direccion?> <?= $cliente->localidad?>
    <br><strong>Fecha de obtención del carnet:</strong> <?= Yii::$app->formatter->asDate($cliente->fecha_permiso_conducir, 'dd') ?>-<?= Yii::$app->formatter->asDate($cliente->fecha_permiso_conducir, 'MM') ?>-<?= Yii::$app->formatter->asDate($cliente->fecha_permiso_conducir, 'yyyy') ?>
    <br><br><strong>1. Descripción del Vehículo:</strong>

    <br><br>El Arrendador alquila al Arrendatario el siguiente vehículo:

    <br><strong>Marca:</strong> <?= $coche->marca?>
    <br><strong>Modelo:</strong> <?= $coche->modelo?>
    <br><strong>Número de Matrícula:</strong> <?= $coche->matricula?>

    <br><br><strong>2. Duración del Alquiler:</strong>

    <br><br>El período de alquiler comenzará el <?= Yii::$app->formatter->asDate($model->fecha_inicio, 'dd') ?>-<?= Yii::$app->formatter->asDate($model->fecha_inicio, 'MM') ?>-<?= Yii::$app->formatter->asDate($model->fecha_inicio, 'yyyy') ?> y finalizará el <?= Yii::$app->formatter->asDate($model->fecha_fin, 'dd') ?>-<?= Yii::$app->formatter->asDate($model->fecha_fin, 'MM') ?>-<?= Yii::$app->formatter->asDate($model->fecha_fin, 'yyyy') ?>

    <br><br><strong>3. Condiciones del Alquiler:</strong>
    <br><br>
    El Arrendatario se compromete a utilizar el vehículo exclusivamente para fines legales y de conformidad con las leyes de tránsito 
    locales. El Arrendatario asume la responsabilidad de cualquier daño causado al vehículo durante el período de alquiler, excepto por 
    desgaste normal. El Arrendatario se compromete a devolver el vehículo en las mismas condiciones en que lo recibió, salvo el desgaste 
    normal.<br><br>
    El Arrendatario se compromete a devolver el vehículo en la fecha y hora acordadas. Cualquier extensión del período de alquiler debe 
    ser acordada por escrito y estar sujeta a disponibilidad. El Arrendatario se compromete a pagar una tarifa de alquiler total de 
    <strong><?= Yii::$app->formatter->asCurrency($model->precio_total, 'EUR') ?> </strong> por el uso del vehículo durante el período acordado. <br><br>El Arrendatario se compromete a 
    pagar por cualquier costo adicional incurrido durante el período de alquiler, como multas de tráfico, peajes, o cargos por limpieza 
    excesiva. El Arrendatario debe ser mayor de 18 años y poseer una licencia de conducir válida y vigente en el 
    momento del alquiler.

    <br><br><strong>4. Responsabilidades del Arrendatario:</strong>

    <br><br>Durante el período de alquiler, el Arrendatario será responsable de mantener el vehículo en buen estado y de acuerdo con las leyes de tráfico locales. El Arrendatario también será responsable de pagar todos los peajes y multas de tráfico incurridos durante el uso del vehículo.

    <br><br><strong>5. Seguro y Responsabilidad:</strong>

    <br><br>El Arrendador proporciona seguro básico para el vehículo, que cubre daños a terceros y responsabilidad civil. El Arrendatario será responsable de cualquier daño al vehículo o a terceros que no esté cubierto por el seguro. Cualquier daño al vehículo se le cobrará al Arrendatario un extra según la gravedad.

    <br><br><strong>6. Restricciones de Uso:</strong>

    <br><br>El Arrendatario utilizará el vehículo únicamente para fines legales y dentro del área geográfica acordada. Se prohíbe el uso del vehículo para carreras, pruebas de velocidad o cualquier otra actividad ilegal.

    <br><br><strong>7. Devolución del Vehículo:</strong>

    <br><br>El Arrendatario devolverá el vehículo en la fecha y hora acordadas al final del período de alquiler. El Arrendador realizará una inspección del vehículo y resolverá cualquier disputa sobre daños o cargos adicionales.

    <br><br><strong>8. Resolución de Disputas:</strong>

    <br><br>Cualquier disputa relacionada con este contrato se resolverá mediante mediación, según las leyes del estado del Estado.
</p>
<br><br>
<p style="text-align: center;">En Santander a <?= Yii::$app->formatter->asDate($model->fecha_inicio, 'dd') ?> <?=$nombreMes?> del <?= Yii::$app->formatter->asDate($model->fecha_inicio, 'yyyy') ?> </p>
<br><br><br><br><br><br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<span><strong>Firma del Arrendador</strong></span>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<span><strong>Firma del Arrendatario</strong></span>  


<?php
else :
    echo "<p>Error: El contrato no se ha encontrado</p>";
endif;
?>
