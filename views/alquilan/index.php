<?php

use app\models\Alquilan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Contratos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquilan-index">

    <h1 class="tituloTablas"><?= Html::encode($this->title) ?></h1>
    
    <div class="alinear-btn">
        <div class="contenedor-contrato">
            <?= Html::a('Nuevo Contrato', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(
                '<img src="' . Yii::getAlias('@web') . '/img/export.png" />',
                ['exportar'], ['class' => 'btn-exportar', 'title' => 'Exportar a Excel']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [       
        [
            'label' => 'Cliente',
            'value' => function ($model) {
                return $model->dniCliente->nombre_completo;
            }
        ],
        [
            'attribute' => 'dni_cliente',
            'headerOptions' => ['style' => 'width: 180px;'],
        ],
        'matricula',
        [
            'attribute' => 'fecha_inicio',
            'format' => ['date', 'php:d-m-Y'], //Formato de fecha
        ],
        [
            'attribute' => 'fecha_fin',
            'format' => ['date', 'php:d-m-Y'],
        ],
        [
            'attribute' => 'precio_total',
            'value' => function ($model) {
                return strpos($model->precio_total, '.') !== false 
                    ? number_format($model->precio_total, 2, ',', '.') 
                    : number_format($model->precio_total, 0, ',', '.');
            },
            'format' => 'raw',
        ],
        ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}']
    ],
]); ?>

</div>
