<?php
use yii\helpers\Html;
use dosamigos\highcharts\HighCharts;

$this->title = 'Estadísticas';
$this->params['breadcrumbs'][] = $this->title;
    
    //Cantidad de contratos que ha realizado cada cliente
    $data1 = [];
    foreach ($consulta1 as $dni) {
        $data1[] = [
            'name' => $dni['nombre_completo'],
            'y' => (float) $dni['count'],
        ];
    }
    
    $chart1 = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => '¿Cuántos coches ha alquilado cada cliente?'],
        'series' => [['name' => 'Total de contratos', 'data' => $data1]],
    ];
    
    //Cantidad de contratos por coche
    $data2 = [];
    foreach ($consulta2 as $matricula) {
        $data2[] = [
            'name' => $matricula['matricula'],
            'y' => (float) $matricula['count'],
        ];
    }
    
    $chart2 = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => '¿Cuántas veces ha sido alquilado cada vehículo?'],
        'series' => [['name' => 'Total de contratos', 'data' => $data2]],
    ];
    
    //Cantidad de reparaciones que ha tenido cada vehiculo
    $data3 = [];
    foreach ($consulta3 as $matricula) {
        $data3[] = [
            'name' => $matricula['matricula'],
            'y' => (float) $matricula['count'],
        ];
    }
    
    $chart3 = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => '¿Cuántos mantenimientos se le ha realizado a cada vehículo?'],
        'series' => [['name' => 'Total de reparaciones', 'data' => $data3]],
    ];
    
    //Cantidad de reparaciones que ha tenido cada vehiculo
    $data4 = [];
    foreach ($consulta4 as $registro) {
        $data4[] = [
            'name' => $registro['marca'],
            'y' => (int)$registro['total']
        ];
    }

    $chart4 = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => '¿Cuántos coches hay por marca?'],
        'series' => [['name' => 'Total de coches', 'data' => $data4]],
    ];
    
    //Cantidad de reparaciones que ha tenido cada vehiculo
    $data5 = [];
    foreach ($consulta5 as $registro) {
        $data5[] = [
            'name' => $registro['nombre_completo'],
            'y' => (int)$registro['total']
        ];
    }

    $chart5 = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => '¿Cuántos clientes ha atendido cada empleado?'],
        'series' => [['name' => 'Total de registros', 'data' => $data5]],
    ];
    
    //Marcas cuyos coches tienen mas kilometros
    $data6 = [];
    foreach ($consulta6 as $registro) {
        $data6[] = [
            'name' => $registro['marca'],
            'y' => (int)$registro['total']
        ];
    }

    $chart6 = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => '¿Cuántos kilómetros han realizado los coches de cada marca?'],
        'series' => [['name' => 'Total de kilómetros', 'data' => $data6]],
    ];
    
?>

<div class="site-index">
    <br>
    <div class="container">
        <div class="row contenedor-estadisticas">
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chart1]); ?>
            </div>
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chart4]); ?>
            </div>
        </div>
        
        <div class="row contenedor-estadisticas" >
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chart3]); ?>
            </div>
            <div class="col-md-5">
                
                <?= HighCharts::widget(['clientOptions' => $chart2]); ?>
            </div>
        </div>
        
        <div class="row contenedor-estadisticas" >
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chart5]); ?>
            </div>
            <div class="col-md-5">               
                <?= HighCharts::widget(['clientOptions' => $chart6]); ?>
            </div>
        </div>
    </div>
</div>    
<?php
$this->registerJsFile('@web/js/highcharts.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/exporting.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/export-data.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerCssFile('@web/css/highcharts.css');