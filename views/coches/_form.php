<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Coches $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="coches-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula')->textInput(['maxlength' => true, 'placeholder' => 'Formato: 1234BVB', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'marca')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'modelo')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'kilometros')->textInput(['onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'potencia')->textInput(['onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'estado')->hiddenInput(['value' => '0'])->label(false) ?>

    <?= $form->field($model, 'precio')->textInput(['onchange' => 'replaceCommaWithDot(this)', 'autocomplete' => 'off']) ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
