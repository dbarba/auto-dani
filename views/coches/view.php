<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Coches $model */

$this->title = 'Coche: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alinear">
    <div class="coches-view">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <p>
            <?= Html::a('Actualizar', ['update', 'matricula' => $model->matricula], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'matricula' => $model->matricula], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro de que quieres eliminar este registro?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'matricula',
                'marca',
                'modelo',
                'color',
                [
                    'attribute' => 'kilometros',
                    'value' => function ($model) {
                        return number_format($model->kilometros, 0, ',', '.');
                    },
                ],
                [
                    'attribute' => 'potencia',
                    'value' => function ($model) {
                        return empty($model->potencia) ? '-' : $model->potencia;
                    },
                ],
                [
                    'label' => 'Estado',
                    'value' => function($model) {
                        return $model->estado ? 'Alquilado' : 'Disponible';
                    },
                ],
                [
                    'attribute' => 'precio',
                    'value' => function ($model) {
                        return strpos($model->precio, '.') !== false 
                            ? number_format($model->precio, 2, ',', '.') 
                            : number_format($model->precio, 0, ',', '.');
                    },
                    'format' => 'raw',
                ], 
                [
                    'label' => 'Número de póliza',
                    //Ternaria para mostrar el numpoliza o el texto indicado en el caso de que no tenga seguro
                    'value' => isset($model->seguros) ? $model->seguros->numero_poliza : 'Sin seguro', 
                ],
            ],
        ]) ?>

    </div>
</div>