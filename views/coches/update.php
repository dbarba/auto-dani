<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Coches $model */

$this->title = 'Actualizar Coche: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->matricula, 'url' => ['view', 'matricula' => $model->matricula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="coches-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>