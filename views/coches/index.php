<?php

use app\models\Coches;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coches-index">

    <h1 class="tituloTablas"><?= Html::encode($this->title) ?></h1>

    <div class="alinear-btn">
        <div class="btn-mantenimiento">
            <?= Html::a('Nuevo Coche', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'matricula',
            'marca',
            'modelo',
            'color',
            [
                'attribute' => 'kilometros',
                'value' => function ($model) {
                    return number_format($model->kilometros, 0, ',', '.');
                },
            ],
            'potencia',
            'estado',
            [
                'attribute' => 'precio',
                'value' => function ($model) {
                    return strpos($model->precio, '.') !== false 
                        ? number_format($model->precio, 2, ',', '.') 
                        : number_format($model->precio, 0, ',', '.');
                },
                'format' => 'raw',
            ], 
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Coches $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'matricula' => $model->matricula]);
                 }
            ],
        ],
    ]); ?>
</div>