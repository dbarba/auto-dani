<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Clientes $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
    
    <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'placeholder' => '12345678A (DNI) o X1234567A (NIE)', 'autocomplete' => 'off'])->label('DNI / NIE') ?>             
    
    <?= $form->field($model, 'fecha_permiso_conducir')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'readonly' => true],
        'clientOptions' => [
            'maxDate' => 0,
            'changeMonth' => true,
            'changeYear' => true,
            'yearRange' => '-80:+0',
            'theme' => 'base', 
            'placeholder' => 'Seleccione fecha',
            'autocomplete' => 'off',
            'beforeShowDay' => new \yii\web\JsExpression('
                function(date) {
                    var day = date.getDay();
                    return [(day != 0), ""];
                }
            ')
        ],
    ])->label('Fecha de obtención del permiso de conducir') ?>
    
    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>
    
    <?= $form->field($model, 'localidad')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?> 

    <div class="form-group">
        <?= Html::submitButton('Siguiente', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


