<?php

use app\models\Clientes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;


/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-index">    
    <h1 class="tituloTablas"><?= Html::encode($this->title) ?></h1>
    
    <div class="alinear-btn">
        <div class="btn-mantenimiento">
            <?= Html::a('Nuevo Cliente', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="row">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>        
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'dni',
            'nombre_completo',
            'localidad',
            'direccion',
            [
            'attribute' => 'fecha_permiso_conducir',
            'format' => ['date', 'php:d-m-Y'],
        ]   ,
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Clientes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dni' => $model->dni]);
                 }
            ],
        ],
    ]); ?>
  
</div>


