<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Clientes $model */

$this->title = 'Cliente: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alinear">
    <div class="clientes-view">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <p>
            <?= Html::a('Actualizar', ['update', 'dni' => $model->dni], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'dni' => $model->dni], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro de que quieres eliminar este registro?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

         <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre_completo',
            'dni',
            'direccion',
            'localidad',
            [
                'attribute' => 'fecha_permiso_conducir',
                'format' => ['date', 'php:d-m-Y'], // Aquí se especifica el formato de la fecha
            ],
            [
                'label' => 'Teléfonos',
                'value' => function($model) {
                    $telefonos = '';
                    foreach ($model->telefonos as $telefono) {
                        $telefonos .= $telefono->telefono . '<br>';
                    }
                    return $telefonos;
                },
                'format' => 'html',
            ],
        ],
    ]) ?>

    </div>
</div>