<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="matricula-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el DNI del cliente a buscar', 'style' => 'width: 292px;', 'autocomplete' => 'off'])->label(false) ?>
            <?php if ($model->hasErrors('dni')): ?>
                <div class="alert alert-danger" style="margin-top: 5px;">
                    <?= $model->getFirstError('dni') ?>
                </div>
            <?php endif; ?>
        </div>
    
        <div class="form-group" style="margin-left: 36px; margin-right: -13px;">
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-secondary btn-buscador']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

