<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Clientes $model */

$this->title = 'Actualizar Cliente: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alinear">
    <div class="clientes-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <br>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>