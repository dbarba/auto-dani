<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Presupuesto;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Presupuesto */
/* @var $options array */

$this->title = 'Auto Dani';

if (Yii::$app->session->hasFlash('presupuestoFormSubmitted')) {
    echo "<p>¡Presupuesto enviado exitosamente!</p>";
}
?>
<div class="homeGeneral container">
    <div class="row row-slider">
        <div class="col p-0">
            <div class="overlay"></div>
            <div class="carousel-overlay">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <?= Html::img('@web/img/homeImagen1.jpg', ['class' => 'd-block w-100', 'alt' => 'First slide']) ?>
                        </div>
                        <div class="carousel-item">
                            <?= Html::img('@web/img/homeImagen2.jpg', ['class' => 'd-block w-100', 'alt' => 'Second slide']) ?>
                        </div>
                        <div class="carousel-item">
                            <?= Html::img('@web/img/homeImagen3.jpg', ['class' => 'd-block w-100', 'alt' => 'Third slide']) ?>
                        </div>
                        <div class="carousel-item">
                            <?= Html::img('@web/img/homeImagen4.jpg', ['class' => 'd-block w-100', 'alt' => 'Quarter slide']) ?>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Siguiente</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="contenedor-tarjetas">
        <div class="card" style="width: 18rem;">
            <?= Html::img('@web/img/cliente.png', ['class' => 'd-block w-100 card-img-top img-cliente', 'alt' => 'Imagen cliente']) ?> 
            <div class="card-body pt-2">
                <?= Html::a('Nuevo Cliente', ['clientes/create'], ['class' => 'btn btn-success w-100 btn-cliente-home']) ?>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <?= Html::img('@web/img/contrato.png', ['class' => 'd-block w-100 card-img-top img-cliente', 'alt' => 'Imagen contrato']) ?> 
            <div class="card-body pt-2">
                <?= Html::a('Nuevo Contrato', ['alquilan/create'], ['class' => 'btn btn-success w-100 btn-cliente-home']) ?>
            </div>
        </div>
    </div>
    <div class="contenedor-abajo">
        <div class="row py-2">
            <div class="col-2"></div>
                <div class="col-8">
                    <h1 class="titulo-presupuesto">Generar presupuesto</h1> 

                    <?php $form = ActiveForm::begin([
                        'id' => 'presupuesto-form',
                    ]); ?>

                    <div class="form-inline-group-container">
                        <div class="form-inline-group">
                            <div class="form-group-inline">
                                <?= $form->field($model, 'matricula')->dropDownList(ArrayHelper::map($options, 'data-precio', 'label'), [
                                    'prompt' => 'Seleccione un coche',
                                    'id' => 'select-matricula-presupuesto',
                                    'style' => 'width: 320px;',
                                ])->label('Matrícula del coche') ?>
                            </div>

                            <?= Html::hiddenInput('Presupuesto[matricula]', '', ['id' => 'hidden-matricula-field-presupuesto']) ?>

                            <div class="form-group-inline">
                                <?= $form->field($model, 'fecha_inicio')->textInput([
                                    'value' => date('d-m-Y'), 
                                    'readonly' => true, 
                                    'style' => 'width: 230px;'
                                ])->label('Fecha de inicio') ?>
                            </div>

                            <div class="form-group-inline">
                                <?= $form->field($model, 'fecha_fin')->widget(\yii\jui\DatePicker::className(), [
                                    'dateFormat' => 'dd-MM-yyyy',
                                    'options' => ['class' => 'form-control', 'readonly' => true, 'style' => 'width: 230px;'],
                                    'clientOptions' => [
                                        'minDate' => 0, // Esto establece la mínima fecha seleccionable como la fecha actual
                                        'maxDate' => '+3m', // Esto establece la máxima fecha seleccionable como tres meses desde la fecha actual
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange' => '-100:+2',
                                        'theme' => 'base',
                                        'placeholder' => 'Seleccione fecha',
                                        'beforeShowDay' => new \yii\web\JsExpression('
                                            function(date) {
                                                var day = date.getDay();
                                                return [(day != 0), ""];
                                            }
                                        ')
                                    ],
                                ])->label('Fecha de fin') ?>
                            </div>
             
                            <div class="form-group-inline">
                                <?= $form->field($model, 'precio_total')->textInput([
                                    'id' => 'precio-total', 
                                    'readonly' => true, 
                                    'style' => 'width: 320px;'
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <?= Html::submitButton('Descargar Presupuesto', ['class' => 'btn btn-success btn-presupuesto']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>




            <div class="col-2"></div>
        </div>
    </div>
</div>
