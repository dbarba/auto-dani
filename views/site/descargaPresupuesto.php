<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Presupuesto */

?>

<?= Html::img('@web/img/logotipo.png', ['id' => 'logoPDF', 'style' => 'float: right; width: 300px; margin: 12px;']) ?>

<div class="pdf-cabecera" style="margin-right: 40px; text-align: center; color: #858585">
    <br>Polígono Industrial de Candina, Nave 4
    <br>Santander, Cantabria
    <br>De lunes a sábado de 9:00 a 14:00
    <br>Teléfono 601472066 / 942710418
</div>

<h2 class="titulo" style="text-align: center;">Presupuesto</h2>

<p>
Datos del vehículo:
<br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Marca:</strong> <?= Html::encode($coche->marca) ?>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Modelo:</strong> <?= Html::encode($coche->modelo) ?>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Número de Matrícula:</strong> <?= Html::encode($model->matricula) ?>


    <br><br>El período de alquiler sería del <?= Yii::$app->formatter->asDate($model->fecha_inicio, 'dd') ?>-<?= Yii::$app->formatter->asDate($model->fecha_inicio, 'MM') ?>-<?= Yii::$app->formatter->asDate($model->fecha_inicio, 'yyyy') ?> y finalizará el <?= Yii::$app->formatter->asDate($model->fecha_fin, 'dd') ?>-<?= Yii::$app->formatter->asDate($model->fecha_fin, 'MM') ?>-<?= Yii::$app->formatter->asDate($model->fecha_fin, 'yyyy') ?>.
    <br><br>En caso de alquiler hay que tener en cuenta lo siguiente:
    <br><br><strong>1.Condiciones del Alquiler:</strong>
    <br><br>
    El Arrendatario se compromete a utilizar el vehículo exclusivamente para fines legales y de conformidad con las leyes de tránsito 
    locales. El Arrendatario asume la responsabilidad de cualquier daño causado al vehículo durante el período de alquiler, excepto por 
    desgaste normal. El Arrendatario se compromete a devolver el vehículo en las mismas condiciones en que lo recibió, salvo el desgaste 
    normal.<br><br>
    El Arrendatario se compromete a devolver el vehículo en la fecha y hora acordadas. Cualquier extensión del período de alquiler debe 
    ser acordada por escrito y estar sujeta a disponibilidad. El Arrendatario se compromete a pagar una tarifa de alquiler total de 
    <strong><?= Yii::$app->formatter->asCurrency($model->precio_total, 'EUR') ?> </strong> por el uso del vehículo durante el período acordado. <br><br>El Arrendatario se compromete a 
    pagar por cualquier costo adicional incurrido durante el período de alquiler, como multas de tráfico, peajes, o cargos por limpieza 
    excesiva. El Arrendatario debe ser mayor de 18 años y poseer una licencia de conducir válida y vigente en el 
    momento del alquiler.

    <br><br><strong>2. Responsabilidades del Arrendatario:</strong>

    <br><br>Durante el período de alquiler, el Arrendatario será responsable de mantener el vehículo en buen estado y de acuerdo con las leyes de tráfico locales. El Arrendatario también será responsable de pagar todos los peajes y multas de tráfico incurridos durante el uso del vehículo.

    <br><br><strong>3. Seguro y Responsabilidad:</strong>

    <br><br>El Arrendador proporciona seguro básico para el vehículo, que cubre daños a terceros y responsabilidad civil. El Arrendatario será responsable de cualquier daño al vehículo o a terceros que no esté cubierto por el seguro. Cualquier daño al vehículo se le cobrará al Arrendatario un extra según la gravedad.

    <br><br><strong>4. Restricciones de Uso:</strong>

    <br><br>El Arrendatario utilizará el vehículo únicamente para fines legales y dentro del área geográfica acordada. Se prohíbe el uso del vehículo para carreras, pruebas de velocidad o cualquier otra actividad ilegal.

    <br><br><strong>5. Devolución del Vehículo:</strong>

    <br><br>El Arrendatario devolverá el vehículo en la fecha y hora acordadas al final del período de alquiler. El Arrendador realizará una inspección del vehículo y resolverá cualquier disputa sobre daños o cargos adicionales.

    <br><br><strong>6. Resolución de Disputas:</strong>

    <br><br>Cualquier disputa relacionada con este contrato se resolverá mediante mediación, según las leyes del estado del Estado.
</p>



