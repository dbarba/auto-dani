<?php

namespace app\controllers;

use Yii;
use app\models\Seguros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SegurosController implements the CRUD actions for Seguros model.
 */
class SegurosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Seguros models.
     *
     * @return string
     */
    public function actionIndex($matricula = null)
    {
        $searchModel = new Seguros(); //Buscador
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $matricula);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Seguros model.
     * @param int $numero_poliza Numero Poliza
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numero_poliza)
    {
        return $this->render('view', [
            'model' => $this->findModel($numero_poliza),
        ]);
    }

    /**
     * Creates a new Seguros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Seguros();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numero_poliza' => $model->numero_poliza]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Seguros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numero_poliza Numero Poliza
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numero_poliza)
    {
        $model = $this->findModel($numero_poliza);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'numero_poliza' => $model->numero_poliza]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Seguros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numero_poliza Numero Poliza
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numero_poliza)
    {
        $this->findModel($numero_poliza)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Seguros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numero_poliza Numero Poliza
     * @return Seguros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numero_poliza)
    {
        if (($model = Seguros::findOne(['numero_poliza' => $numero_poliza])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
