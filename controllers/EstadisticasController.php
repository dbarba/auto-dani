<?php

namespace app\controllers;

use app\models\Alquilan;
use app\models\Mantenimientos;
use app\models\Coches;
use app\models\Atienden;
use app\models\Clientes;
use app\models\Empleados;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * EmpleadosController implements the CRUD actions for Empleados model.
 */
class EstadisticasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Empleados models.
     *
     * @return string
     */
    public function actionIndex()
    {
        //Clientes con más contratos
        $consulta1 = Alquilan::find()
            ->select(['nombre_completo', 'dni_cliente', 'COUNT(dni_cliente) AS count'])
            ->innerJoin('clientes', 'alquilan.dni_cliente = clientes.dni') // Asegúrate de tener la relación definida en el modelo
            ->groupBy('dni_cliente')
            ->orderBy(['count' => SORT_DESC]) // Ordenar por cantidad de contratos en orden descendente
            ->limit(10) // Limitar a los 10 primeros clientes
            ->asArray()
            ->all();

        //Coches con más contratos
        $consulta2 = Alquilan::find()->select(['matricula', 'COUNT(*) AS count'])
        ->groupBy('matricula')
        ->orderBy(['count' => SORT_DESC]) // Ordenar por cantidad de contratos en orden descendente
        ->limit(10) // Limitar a los 10 primeros clientes
        ->asArray()
        ->all();
        
        //Coches con más reparaciones
        $consulta3 = Mantenimientos::find()->select(['matricula', 'COUNT(*) AS count'])
        ->groupBy('matricula')
        ->orderBy(['count' => SORT_DESC]) // Ordenar por cantidad de reparaciones en orden descendente
        ->limit(10) // Limitar a los 10 primeros clientes
        ->asArray()
        ->all();
        
        //Marcas con mas coches
        $consulta4 = Coches::find()
            ->select(['marca', 'COUNT(*) AS total'])
            ->groupBy('marca')
            ->orderBy(['total' => SORT_DESC])
            ->limit(10)
            ->asArray()
            ->all(); 
        
        //Cuantos clientes ha atendido cada empleado
        $consulta5 = Atienden::find()
            ->select(['id_empleado', 'nombre_completo', 'COUNT(*) AS total'])
            ->innerJoin('empleados', 'atienden.id_empleado = empleados.id')
            ->groupBy('id_empleado')
            ->orderBy(['total' => SORT_DESC]) // Ordenar por cantidad de reparaciones en orden descendente
            ->limit(10) // Limitar a los 10 primeros clientes
            ->asArray()
            ->all();
 
        
        //Kilómetros totales agrupados por marcas
        $consulta6 = Coches::find()->select(['marca', 'SUM(kilometros) AS total'])
        ->groupBy('marca')
        ->orderBy(['total' => SORT_DESC]) // Ordenar por cantidad de contratos en orden descendente
        ->limit(10)
        ->asArray()
        ->all();     
        
        return $this->render('index', [
            'consulta1' => $consulta1,
            'consulta2' => $consulta2,
            'consulta3' => $consulta3,
            'consulta4' => $consulta4,
            'consulta5' => $consulta5,
            'consulta6' => $consulta6
        ]);
    }

    /**
     * Displays a single Empleados model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    
}
