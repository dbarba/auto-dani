<?php

namespace app\controllers;

use Yii;
use app\models\Clientes;
use app\models\Telefonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TelefonosController implements the CRUD actions for Telefonos model.
 */
class TelefonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Telefonos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Telefonos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Telefonos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Telefonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($dni)
    {
        $clienteData = Yii::$app->session->get('clienteData');

        $model = new Telefonos();
        $model->dni_cliente = $dni;

        if ($this->request->isPost) {
            $telefonosPost = $this->request->post('Telefonos')['telefono'];
            $success = true;

            //Validar que al menos uno de los teléfonos no esté vacío
            if (empty($telefonosPost[0])) {
                $model->addError('telefono', 'El primer teléfono es obligatorio');
                $success = false;
            }

            //Guardar teléfonos si la validación es correcta
            if ($success) {
                foreach ($telefonosPost as $telefono) {
                    if (!empty($telefono)) {
                        $newTelefono = new Telefonos();
                        $newTelefono->dni_cliente = $dni;
                        $newTelefono->telefono = $telefono;
                        if (!$newTelefono->save()) {
                            $success = false;
                        }
                    }
                }

                if ($success) {
                    Yii::$app->session->remove('clienteData');
                    return $this->redirect(['clientes/view', 'dni' => $dni]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }




    /**
     * Updates an existing Telefonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni)
    {
        $cliente = Clientes::findOne($dni);
        $telefonos = Telefonos::findAll(['dni_cliente' => $dni]);

        if ($this->request->isPost) {
            $telefonosPost = $this->request->post('Telefonos');
            $success = true;

            // Validar que el primer teléfono no esté vacío
            if (empty($telefonosPost[0]['telefono'])) {
                $success = false;
                $telefonos[0]->addError('telefono', 'El primer teléfono es obligatorio');
            }

            if ($success) {
                foreach ($telefonos as $index => $telefono) {
                    if (isset($telefonosPost[$index]['telefono'])) {
                        $telefono->telefono = $telefonosPost[$index]['telefono'];
                        if (!$telefono->save()) {
                            $success = false;
                        }
                    } else {
                        $telefono->delete();
                    }
                }

                for ($i = count($telefonos); $i < count($telefonosPost); $i++) {
                    if (!empty($telefonosPost[$i]['telefono'])) {
                        $nuevoTelefono = new Telefonos();
                        $nuevoTelefono->dni_cliente = $dni;
                        $nuevoTelefono->telefono = $telefonosPost[$i]['telefono'];
                        if (!$nuevoTelefono->save()) {
                            $success = false;
                            $telefonos[] = $nuevoTelefono; // Añadir el nuevo modelo con errores
                        }
                    }
                }

                if ($success) {
                    return $this->redirect(['clientes/view', 'dni' => $dni]);
                }
            }
        }

        return $this->render('update', [
            'model' => $cliente,
            'telefonos' => $telefonos,
        ]);
    }

    /**
     * Deletes an existing Telefonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Telefonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Telefonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Telefonos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
