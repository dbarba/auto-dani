<?php

namespace app\controllers;

use Yii;
use app\models\Clientes;
use app\models\Telefonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Clientes models.
     *
     * @return string
     */
    public function actionIndex($dni = null)
    {
        $searchModel = new Clientes(); //Buscador
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dni);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clientes model.
     * @param string $dni Dni
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni),
        ]);
    }

    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Clientes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                //Convertir el formato de fecha adecuado
                $fecha = \DateTime::createFromFormat('d-m-Y', $model->fecha_permiso_conducir);
                $model->fecha_permiso_conducir = $fecha->format('Y-m-d');

                if ($model->save()) {
                    return $this->redirect(['telefonos/create', 'dni' => $model->dni]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni Dni
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni)
    {
        $model = $this->findModel($dni);

        if ($model->load(Yii::$app->request->post())) {
            //Convertir el formato de fecha adecuado
            $fecha = \DateTime::createFromFormat('d-m-Y', $model->fecha_permiso_conducir);
            if ($fecha) {
                $model->fecha_permiso_conducir = $fecha->format('Y-m-d');
            }

            if ($model->save()) {
                return $this->redirect(['telefonos/update', 'dni' => $model->dni]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni Dni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni)
    {
        $this->findModel($dni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni Dni
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni)
    {
        if (($model = Clientes::findOne(['dni' => $dni])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
