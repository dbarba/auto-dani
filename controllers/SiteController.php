<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Presupuesto;
use app\models\Coches;
use app\models\Seguros;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Presupuesto();
        $options = [];

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->validate()) {
                //Convertur las fechas al formato adecuado
                $fecha1 = \DateTime::createFromFormat('d-m-Y', $model->fecha_inicio);
                $model->fecha_inicio = $fecha1->format('Y-m-d');
                $fecha2 = \DateTime::createFromFormat('d-m-Y', $model->fecha_fin);
                $model->fecha_fin = $fecha2->format('Y-m-d');

                //Llamar al método para generar el PDF
                return $this->generarPDF($model);
            }
        } else {
            //Obtener opciones para el desplegable y guardarlas en un Array
            $coches = Coches::find()
                ->joinWith('seguros')
                ->where(['coches.estado' => 0])
                ->andWhere(['is not', 'seguros.matricula', null])
                ->all();

            foreach ($coches as $coche) {
                $options[$coche->matricula] = [
                    'data-precio' => $coche->precio,
                    'label' => $coche->matricula . ' - ' . $coche->marca . ' ' . $coche->modelo
                ];
            }
        }

        return $this->render('index', [
            'model' => $model,
            'options' => $options,
        ]);
    }

    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionPresupuesto()
    {
        $model = new PresupuestoForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            Yii::$app->session->setFlash('presupuestoFormSubmitted');
            
            //Guardar el presupuesto
            Yii::$app->session->set('presupuestoData', [
                'matricula' => $model->matricula,
                'fecha_inicio' => $model->fecha_inicio,
                'fecha_fin' => $model->fecha_fin,
                'precio_total' => $model->precio_total,
            ]);

            return $this->refresh();
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
    
    public function generarPDF($model)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');

        $coche = Coches::findOne(['matricula' => $model->matricula]);

        //Verificar si se encontró el coche
        if ($coche !== null) {
            $content = $this->renderPartial('descargaPresupuesto', [
                'model' => $model,
                'coche' => $coche, 
            ]);
        }
        // Configurar el PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'methods' => [
                'SetTitle' => 'Descargar presupuesto',
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }
}
