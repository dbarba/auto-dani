<?php

namespace app\controllers;

use app\models\Alquilan;
use app\models\Coches;
use app\models\Clientes;
use app\models\Seguros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\web\Response;


/**
 * AlquilanController implements the CRUD actions for Alquilan model.
 */
class AlquilanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Alquilan models.
     *
     * @return string
     */
    public function actionIndex($dni_cliente = null)
    {
        $searchModel = new Alquilan(); //Buscador
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dni_cliente);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alquilan model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alquilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */

    
    public function actionCreate()
    {
        $model = new Alquilan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                //Convertir las fechas al formato correcto
                $fecha1 = \DateTime::createFromFormat('d-m-Y', $model->fecha_inicio);
                $model->fecha_inicio = $fecha1->format('Y-m-d');
                $fecha2 = \DateTime::createFromFormat('d-m-Y', $model->fecha_fin);
                $model->fecha_fin = $fecha2->format('Y-m-d');
                
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            //Opciones del desplegable, guardarlas en un array
            $model->loadDefaultValues();
            $coches = Coches::find()
                ->joinWith('seguros')  
                ->where(['coches.estado' => 0])
                ->andWhere(['is not', 'seguros.matricula', null])  //Asegurarse de que la matricula no es nula en seguras
                ->all();
            
            $options = [];
            foreach ($coches as $coche) {
                $options[$coche->matricula] = [
                    'data-precio' => $coche->precio,
                    'label' => $coche->matricula . ' - ' . $coche->marca . ' ' . $coche->modelo
                ];
            }
            return $this->render('create', [
                'model' => $model,
                'options' => $options,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Alquilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Deletes an existing Alquilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alquilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Alquilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alquilan::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    //Descargar un contrato en PDF
    public function actionDescargar($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');
        
        $model = Alquilan::findOne($id);

        //Verificar si el modelo se encontró correctamente
        if (!$model) {
            throw new NotFoundHttpException('El contrato solicitado no se ha encontrado.');
        }
        
        $coche = Coches::findOne(['matricula' => $model->matricula]);

        $cliente = Clientes::findOne(['dni' => $model->dni_cliente]);

        //Configura el PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('descargaPdf', ['model' => $model,'coche' => $coche,'cliente' => $cliente,]),
            'methods' => [
                'SetTitle' => 'Descargar contrato',  
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }
    
    //Exportar los contratos a Excel
    public function actionExportar()
    {
        $contratos = Alquilan::find()->all();
        
        $spreadsheet = new Spreadsheet();
        
        //Crear una nueva hoja de cálculo
        $sheet = $spreadsheet->getActiveSheet();
        
        //Agregar encabezados
        $sheet->setCellValue('A1', 'ID');
        $sheet->setCellValue('B1', 'DNI Cliente');
        $sheet->setCellValue('C1', 'Matrícula');
        $sheet->setCellValue('D1', 'Fecha Inicio');
        $sheet->setCellValue('E1', 'Fecha Fin');
        $sheet->setCellValue('F1', 'Precio Total');
        
        //Establecer negrita para las celdas de la cabecera
        $boldFontStyle = [
            'font' => [
                'bold' => true,
            ],
        ];
        
        $sheet->getStyle('A1:F1')->applyFromArray($boldFontStyle);
        
        //Ajustar anchos de columnas
        $sheet->getColumnDimension('A')->setWidth(6);  //Ajustar el ancho de la columna A
        $sheet->getColumnDimension('B')->setWidth(15);  //Ajustar el ancho de la columna B
        $sheet->getColumnDimension('C')->setWidth(12);  //Ajustar el ancho de la columna C
        $sheet->getColumnDimension('D')->setWidth(12);  //Ajustar el ancho de la columna D
        $sheet->getColumnDimension('E')->setWidth(12);  //Ajustar el ancho de la columna E
        $sheet->getColumnDimension('F')->setWidth(10);  //Ajustar el ancho de la columna F

        //Agregar datos
        $row = 2;
        foreach ($contratos as $contrato) {
            $sheet->setCellValue('A' . $row, $contrato->id);
            $sheet->setCellValue('B' . $row, $contrato->dni_cliente);
            $sheet->setCellValue('C' . $row, $contrato->matricula);
            $sheet->setCellValue('D' . $row, date('d-m-Y', strtotime($contrato->fecha_inicio)));
            $sheet->setCellValue('E' . $row, date('d-m-Y', strtotime($contrato->fecha_fin)));
            $sheet->setCellValue('F' . $row, $contrato->precio_total);
            $row++;
        }
        
         //Establecer la ubicación de almacenamiento temporal
        $tempFilePath = Yii::getAlias('@app/runtime') . '/contratos.xlsx';
        
        //Crear el escritor
        $writer = new Xlsx($spreadsheet);
        
        //Guardar el archivo
        $writer->save($tempFilePath);
        
        //Descargar el archivo
        Yii::$app->response->sendFile($tempFilePath, 'contratos.xlsx')->send();
        unlink($tempFilePath); //Eliminar el archivo temporal
    }
    
    public function actionPrice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $matricula = Yii::$app->request->post('matricula');
        $coche = Coches::findOne(['matricula' => $matricula]);

        if ($coche !== null) {
            return ['success' => true, 'precio' => $coche->precio];
        } else {
            return ['success' => false, 'message' => 'Vehículo no encontrado'];
        }
    }
  
}
