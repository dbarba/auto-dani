<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "seguros".
 *
 * @property int $numero_poliza
 * @property float $precio
 * @property string $matricula
 *
 * @property Coches $matricula0
 */
class Seguros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seguros';
    }

    /**
     * {@inheritdoc}
     */
    //Validaciones
    public function rules()
    {
        return [
            [['numero_poliza'], 'required', 'message' => 'Debe de introducir un número de póliza de al menos 6 dígitos'],
            [['precio'], 'required', 'message' => 'Debe de introducir un precio'],
            [['matricula'], 'required', 'message' => 'Debe de seleccionar una matrícula'],
            [['numero_poliza'], 'integer', 'min' => 100000, 'max' => 99999999, 'tooSmall' => 'El número de póliza debe tener al menos 6 dígitos', 'tooBig' => 'El número de póliza debe tener como máximo 8 dígitos'],
            [['matricula'], 'string', 'max' => 7],
            [['matricula'], 'unique'],
            [['numero_poliza'], 'unique'],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::class, 'targetAttribute' => ['matricula' => 'matricula']],
            [['precio'], 'validatePrecio'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numero_poliza' => 'Número de Poliza',
            'precio' => 'Precio (€)',
            'matricula' => 'Matrícula',
        ];
    }

    /**
     * Gets query for [[Matricula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula0()
    {
        return $this->hasOne(Coches::class, ['matricula' => 'matricula']);
    }
    
    //Buscador
    public function search($matricula)
    {
        $query = Seguros::find();

        if (!is_null($matricula)) {
            $query->andFilterWhere(['matricula' => $matricula]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
    
    //Reemplazar comas por puntos
    public function beforeValidate()
    {
        $this->precio = str_replace(',', '.', $this->precio);
        return parent::beforeValidate();
    }
    
    //Validar precio
    public function validatePrecio($attribute, $params)
    {
        $value = $this->$attribute;

        //Verificar si el valor contiene letras
        if (!preg_match('/^\d*\.?\d+$/', $value)) {
            $this->addError($attribute, 'El precio solo puede contener números mayores que 0 (enteros o decimales)');
            return;
        }
        
        //Verificar los decimales del precio
        if (preg_match('/^\d+\.\d{3,}$/', $value)) {
            $this->addError($attribute, 'El precio solo puede tener como máximo dos decimales');
        }

        //Verificar si el valor tiene más de dos decimales
        if (preg_match('/^\d+\.\d{3,}$/', $value)) {
            $this->addError($attribute, 'El precio solo puede tener como máximo dos decimales');
        }
        
        // Verificar si el valor es menor que 5000
        if ($value >= 5000) {
            $this->addError($attribute, 'El precio debe ser menor que 5.000');
        }
       
    }
    
}
