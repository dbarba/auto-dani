<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "mantenimientos".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int $kilometros
 * @property string $fecha
 * @property string $matricula
 *
 * @property Coches $matricula0
 */
class Mantenimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mantenimientos';
    }

    /**
     * {@inheritdoc}
     */
    //Validaciones
    public function rules()
    {
        return [
            [['kilometros'], 'required', 'message' => 'Debe de introducir los kilómetros del vehículo'],
            [['fecha'], 'required', 'message' => 'Debe de seleccionar una fecha'],
            [['matricula'], 'required', 'message' => 'Debe de seleccionar una matrícula'],
            [['kilometros'], 'integer'],
            [['fecha'], 'safe'],
            [['descripcion'], 'string', 'max' => 700],
            ['kilometros', 'integer', 'max' => 999999, 'tooBig' => 'El máximo de kilómetros permitido es 999.999'],
            [['matricula'], 'string', 'max' => 7],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::class, 'targetAttribute' => ['matricula' => 'matricula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'kilometros' => 'Kilómetros',
            'fecha' => 'Fecha de realización',
            'matricula' => 'Matrícula',
        ];
    }

    /**
     * Gets query for [[Matricula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula0()
    {
        return $this->hasOne(Coches::class, ['matricula' => 'matricula']);
    }
    
    //Buscador
    public function search($matricula)
    {
        $query = Mantenimientos::find();

        // Ajustar la condición para manejar cuando no se proporciona una matrícula
        if ($matricula !== null) {
            $query->andFilterWhere(['matricula' => $matricula]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    //Eliminar espacios antes de guardar
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->descripcion = trim($this->descripcion);
            return true;
        }
        return false;
    }
    
    
}
