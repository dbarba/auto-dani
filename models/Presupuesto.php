<?php

namespace app\models;

use yii\base\Model;

class Presupuesto extends Model
{
    public $matricula;
    public $fecha_inicio;
    public $fecha_fin;
    public $precio_total;

   
    //Validaciones
    public function rules()
    {
       return [
           ['matricula', 'required', 'message' => 'Debe seleccionar una matrícula'],
           ['matricula', 'string', 'max' => 9, 'tooLong' => 'La matrícula no puede tener más de 9 caracteres'],

           [['fecha_inicio', 'fecha_fin'], 'required', 'message' => 'Debe seleccionar una fecha'],
           [['fecha_inicio', 'fecha_fin'], 'date', 'format' => 'php:d-m-Y', 'message' => 'El formato de fecha no es válido'],

           ['precio_total', 'required', 'message' => 'Este campo no puede estar vacío'],
           ['precio_total', 'number', 'message' => 'El precio total debe ser un número'],
       ];
    }


    public function attributeLabels()
    {
        return [
            'matricula' => 'Matrícula',
            'fecha_inicio' => 'Fecha de Inicio',
            'fecha_fin' => 'Fecha de Fin',
            'precio_total' => 'Precio Total (€)',
        ];
    }
}
