<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "alquilan".
 *
 * @property int $id
 * @property int $id_cliente
 * @property int $id_coche
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property float $precio_total
 *
 * @property Clientes $cliente
 * @property Coches $coche
 */
class Alquilan extends \yii\db\ActiveRecord
{
    public $precio;

    public static function tableName()
    {
        return 'alquilan';
    }
    
    //Validaciones
    public function rules()
    {
        return [
            [['dni_cliente'], 'required', 'message' => 'Debe de seleccionar un cliente'],
            [['matricula'], 'required', 'message' => 'Debe de seleccionar una matrícula'],
            [['fecha_inicio'], 'required', 'message' => 'Debe de seleccionar una fecha'],
            [['fecha_fin'], 'required', 'message' => 'Debe de seleccionar una fecha'],
            [['precio_total'], 'required', 'message' => 'El precio no puede estar vácio'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['precio_total'], 'number'],
            [['dni_cliente'], 'string', 'max' => 9],
            [['matricula'], 'string', 'max' => 7],
            [['dni_cliente', 'matricula'], 'unique', 'targetAttribute' => ['dni_cliente', 'matricula']],
            [['dni_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::class, 'targetAttribute' => ['dni_cliente' => 'dni']],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::class, 'targetAttribute' => ['matricula' => 'matricula']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni_cliente' => 'DNI / NIE Cliente',
            'matricula' => 'Matrícula',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'precio_total' => 'Precio Total (€)',
        ];
    }

//    public function beforeSave($insert)
//    {
//        if (parent::beforeSave($insert)) {
//            $this->matricula = substr($this->matricula, 0, 7);
//            return true;
//        }
//        return false;
//    }

    public function getDniCliente()
    {
        return $this->hasOne(Clientes::class, ['dni' => 'dni_cliente']);
    }

    public function getMatricula0()
    {
        return $this->hasOne(Coches::class, ['matricula' => 'matricula']);
    }
    
    //Buscador
    public function search($dni_cliente)
    {
        $query = Alquilan::find();

        if (!is_null($dni_cliente)) {
            $query->andFilterWhere(['dni_cliente' => $dni_cliente]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
    
}
