<?php

namespace app\models;

use Yii;
use app\models\Empleado;
use app\models\Cliente;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "atienden".
 *
 * @property int $id
 * @property int $id_empleado
 * @property string $dni_cliente
 * @property string $fecha
 *
 * @property Clientes $dniCliente
 * @property Empleados $empleado
 */
class Atienden extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atienden';
    }

    /**
     * {@inheritdoc}
     */
    //Validaciones
    public function rules()
    {
        return [
        [['id_empleado'], 'required', 'message' => 'Debe de seleccionar un empleado'],
        [['dni_cliente'], 'required', 'message' => 'Debe de seleccionar un cliente'],
        [['fecha'], 'required', 'message' => 'Debe de seleccionar una fecha'],
        [['id_empleado'], 'integer'],
        [['fecha'], 'safe'],
        [['dni_cliente'], 'string', 'max' => 9],
        [['id_empleado', 'dni_cliente'], 'unique', 'targetAttribute' => ['id_empleado', 'dni_cliente']],
        [['dni_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::class, 'targetAttribute' => ['dni_cliente' => 'dni']],
        [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::class, 'targetAttribute' => ['id_empleado' => 'id']],
    ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_empleado' => 'ID Empleado',
            'dni_cliente' => 'DNI / NIE Cliente',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[DniCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCliente()
    {
        return $this->hasOne(Clientes::class, ['dni' => 'dni_cliente']);
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleados::class, ['id' => 'id_empleado']);
    }
    
    //Buscador
    public function search($dni_cliente)
    {
        $query = Atienden::find();

        if (!is_null($dni_cliente)) {
            $query->andFilterWhere(['dni_cliente' => $dni_cliente]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
    
}
