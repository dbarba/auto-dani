<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "coches".
 *
 * @property string $matricula
 * @property string $marca
 * @property string $modelo
 * @property string|null $color
 * @property int $kilometros
 * @property int|null $potencia
 * @property int $estado
 * @property float $precio
 *
 * @property Alquilan[] $alquilans
 * @property Clientes[] $dniClientes
 * @property Mantenimientos[] $mantenimientos
 * @property Seguros $seguros
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    //Validaciones
    public function rules()
    {
        return [
            [['matricula'], 'required', 'message' => 'Debe de introducir la matrícula'],
            [['marca'], 'required', 'message' => 'Debe de introducir la marca'],
            [['modelo'], 'required', 'message' => 'Debe de introducir el modelo'],
            [['kilometros'], 'required', 'message' => 'Debe de introducir los kilómetros'],
            [['estado'], 'required'],
            [['precio'], 'required', 'message' => 'Debe de introducir un precio'],
            [['kilometros', 'potencia', 'estado'], 'integer'],
            [['matricula'], 'string', 'max' => 7],
            [['marca'], 'string', 'max' => 30],
            [['modelo'], 'string', 'max' => 50],
            [['color'], 'string', 'max' => 25],
            [['matricula'], 'unique'],
            
            [['matricula'], 'match', 'pattern' => '/^[0-9]{4}[BCDFGHJKLMbcdfghjklm][BCDFGHJKLMNÑPQRSTVWXYZbcdfghjklmnñpqrstvwxyz]{2}$/i', 'message' => 'El formato correcto es 1111BVB'],
            ['marca', 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑëË\s-]+$/', 'message' => 'La marca no puede contener números'],
            [['marca'], 'string', 'min' => 2, 'tooShort' => 'La marca debe tener al menos 2 caracteres'],  
            [['modelo'], 'string', 'min' => 1, 'tooShort' => 'El modelo debe tener al menos 2 caracteres'],
            ['color', 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑ\s-]+$/', 'message' => 'El color sólo debe de contener letras'],
            [['color'], 'string', 'min' => 2, 'tooShort' => 'El color debe tener al menos 2 caracteres'],
            ['kilometros', 'integer', 'max' => 999999, 'tooBig' => 'El máximo de kilómetros permitido es 999.999'],
            [['potencia'], 'integer', 'max' => 9999, 'tooBig' => 'La potencia debe tener como máximo 4 números'], 
            [['precio'], 'validatePrecio'],        
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'matricula' => 'Matrícula',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'color' => 'Color',
            'kilometros' => 'Kilómetros',
            'potencia' => 'Potencia (CV)',
            'estado' => 'Estado',
            'precio' => 'Precio (€)',
        ];
    }

    /**
     * Gets query for [[Alquilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquilans()
    {
        return $this->hasMany(Alquilan::class, ['matricula' => 'matricula']);
    }

    /**
     * Gets query for [[DniClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniClientes()
    {
        return $this->hasMany(Clientes::class, ['dni' => 'dni_cliente'])->viaTable('alquilan', ['matricula' => 'matricula']);
    }

    /**
     * Gets query for [[Mantenimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientos()
    {
        return $this->hasMany(Mantenimientos::class, ['matricula' => 'matricula']);
    }

    /**
     * Gets query for [[Seguros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeguros()
    {
        return $this->hasOne(Seguros::class, ['matricula' => 'matricula']);
    }

    //Eliminar espacios antes de guardar
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->marca = trim($this->marca);
            $this->modelo = trim($this->modelo);
            $this->color = trim($this->color);

            //Reemplazar comas por puntos
            $this->precio = str_replace(',', '.', $this->precio);

            return true;
        }
        return false;
    }

    
    //Poner las letras de la matrícula en mayúsculas
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //Convertir las letras de la matrícula a mayúsculas antes de guardar
            $this->matricula = strtoupper($this->matricula);
            return true;
        }
        return false;
    }
    
    //Buscador
    public function search($matricula)
    {
        $query = Coches::find();

        if (!is_null($matricula)) {
            $query->andFilterWhere(['matricula' => $matricula]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
    
    //Validar el campo precio
    public function validatePrecio($attribute, $params)
    {
        $value = $this->$attribute;

        // Verificar si el valor contiene letras
        if (!preg_match('/^\d*\.?\d+$/', $value)) {
            $this->addError($attribute, 'El precio solo puede contener números mayores que 0 (enteros o decimales)');
            return;
        }

        // Verificar si el valor tiene más de dos decimales
        if (preg_match('/^\d+\.\d{3,}$/', $value)) {
            $this->addError($attribute, 'El precio solo puede tener como máximo dos decimales');
        }
        
        // Verificar si el valor es mayor que 0
        if ($value <= 0) {
            $this->addError($attribute, 'El precio debe ser mayor que 0');
        }
        
        // Verificar si el valor es menor que 5000
        if ($value >= 5000) {
            $this->addError($attribute, 'El precio debe ser menor que 5.000');
        }
       
    }
}
