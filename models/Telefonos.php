<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string $dni_cliente
 * @property string $telefono
 *
 * @property Clientes $dniCliente
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    //Validar teléfonos
    public function rules()
    {
        return [
            [['dni_cliente'], 'required'],
            [['dni_cliente'], 'string', 'max' => 9],
            [['telefono'], 'string', 'max' => 20],
            [['dni_cliente', 'telefono'], 'unique', 'targetAttribute' => ['dni_cliente', 'telefono']],
            [['dni_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::class, 'targetAttribute' => ['dni_cliente' => 'dni']],
            ['telefono', 'validarTelefono'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['dni_cliente', 'telefono'];
        $scenarios['update'] = ['dni_cliente', 'telefono'];  // No obligamos a 'telefono'
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni_cliente' => 'DNI Cliente',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[DniCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCliente()
    {
        return $this->hasOne(Clientes::class, ['dni' => 'dni_cliente']);
    }
    
    public function validarTelefono($attribute, $params)
    {
        //Expresiones regulares para diferentes formatos de teléfono
        $expresiones = [
            'es' => '/^[6-9]\d{8}$/',
            'en' => '/^\+44[7-9]\d{9}$/',
            'de' => '/^\+49[1-5]\d{10}$/',
        ];

        //Comprobar si el teléfono coincide con alguno de los formatos
        $valido = false;
        foreach ($expresiones as $expresion) {
            if (preg_match($expresion, $this->$attribute)) {
                $valido = true;
                break;
            }
        }

        //Si el teléfono no coincide con ningún formato, agregar un error
        if (!$valido) {
            $this->addError($attribute, 'El formato del teléfono no es válido.');
        }
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //Eliminar espacios en blanco 
            $this->telefono = trim($this->telefono);

            return true;
        }
        return false;
    }
    
}
