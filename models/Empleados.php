<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "empleados".
 *
 * @property int $id
 * @property string $nombre_completo
 *
 * @property Atienden[] $atiendens
 * @property Clientes[] $dniClientes
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    //Validaciones
    public function rules()
    {
        return [
            [['nombre_completo'], 'required', 'message' => 'Debe de introducir el nombre del empleado (no se admiten números)'],
            [['nombre_completo'], 'string', 'max' => 70, 'tooLong' => 'El nombre es demasiado largo'],
            [['nombre_completo'], 'string', 'min' => 5, 'tooShort' => 'El nombre es demasiado corto'],
            
            ['nombre_completo', 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑª\s-]+$/', 'message' => 'El nombre sólo debe de contener letras'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_completo' => 'Nombre Completo',
        ];
    }

    /**
     * Gets query for [[Atiendens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAtiendens()
    {
        return $this->hasMany(Atienden::class, ['id_empleado' => 'id']);
    }

    /**
     * Gets query for [[DniClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniClientes()
    {
        return $this->hasMany(Clientes::class, ['dni' => 'dni_cliente'])->viaTable('atienden', ['id_empleado' => 'id']);
    }
    
    //Buscador
    public function search($nombre)
    {
        $query = Empleados::find();

        if (!is_null($nombre)) {
            $query->andFilterWhere(['nombre_completo' => $nombre]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
    //Eliminar los espacios antes de guardar 
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->nombre_completo = trim($this->nombre_completo);
            return true;
        }
        return false;
    }
    
}
