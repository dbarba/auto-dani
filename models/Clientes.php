<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "clientes".
 *
 * @property string $dni
 * @property string $nombre_completo
 * @property string $localidad
 * @property string $direccion
 * @property string $fecha_permiso_conducir
 *
 * @property Alquilan[] $alquilans
 * @property Atienden[] $atiendens
 * @property Empleados[] $empleados
 * @property Coches[] $matriculas
 * @property Telefonos[] $telefonos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    //Validaciones
    public function rules()
    {
        return [
            [['dni'], 'required', 'message' => 'Debe de introducir un DNI o NIE'],
            [['nombre_completo'], 'required', 'message' => 'Debe de introducir el nombre del cliente (no se admiten números)'],
            [['localidad'], 'required', 'message' => 'Debe de introducir la localidad de residencia del cliente'],
            [['direccion'], 'required', 'message' => 'Debe de introducir la dirección de residencia del cliente'],
            [['fecha_permiso_conducir'], 'required', 'message' => 'Debe de seleccionar una fecha'],
            [['fecha_permiso_conducir'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['nombre_completo'], 'string', 'max' => 70, 'tooLong' => 'El nombre es demasiado largo'],
            [['nombre_completo'], 'string', 'min' => 5, 'tooShort' => 'El nombre es demasiado corto'],
            [['localidad'], 'string', 'max' => 40],
            [['direccion'], 'string', 'max' => 100],
            [['dni'], 'unique'],
            
            ['localidad', 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑºª\s-]+$/i', 'message' => 'No se admiten números'],
            [['localidad'], 'string', 'min' => 3, 'tooShort' => 'Demasiado corto'],
            [['direccion'], 'string', 'max' => 100],
            [['direccion'], 'string', 'min' => 5, 'tooShort' => 'La direcció es demasiado corta'],
            ['nombre_completo', 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑª\s-]+$/', 'message' => 'El nombre completo solo puede contener letras'],
            [['nombre_completo'], 'string', 'max' => 70, 'tooLong' => 'El nombre es demasiado largo'],
            [['nombre_completo'], 'string', 'min' => 3, 'tooShort' => 'El nombre es demasiado corto'],
            ['dni', 'match', 'pattern' => '/^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]$|^[XYZxyz][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]$/i', 'message' => 'Formato incorrecto (12345678G o X1234567Z)'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'DNI / NIE',
            'nombre_completo' => 'Nombre Completo',
            'localidad' => 'Localidad',
            'direccion' => 'Dirección',
            'fecha_permiso_conducir' => 'Fecha de obtención del carnet',
        ];
    }

    /**
     * Gets query for [[Alquilans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquilans()
    {
        return $this->hasMany(Alquilan::class, ['dni_cliente' => 'dni']);
    }

    /**
     * Gets query for [[Atiendens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAtiendens()
    {
        return $this->hasMany(Atienden::class, ['dni_cliente' => 'dni']);
    }

    /**
     * Gets query for [[Empleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleados::class, ['id' => 'id_empleado'])->viaTable('atienden', ['dni_cliente' => 'dni']);
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Coches::class, ['matricula' => 'matricula'])->viaTable('alquilan', ['dni_cliente' => 'dni']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['dni_cliente' => 'dni']);
    }
    
    //Eliminar espacios antes de guardar
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->nombre_completo = trim($this->nombre_completo);
            $this->direccion = trim($this->direccion);
            $this->localidad = trim($this->localidad);

            return true;
        }
        return false;
    }
    
    //Buscador
    public function search($dni)
    {
        $query = Clientes::find();

        if (!is_null($dni)) {
            $query->andFilterWhere(['dni' => $dni]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
    
}
