//Cambiar el valor de 0 y 1 en la tabla de coches, columna estados
const cochesAlquilados = {
    '0': 'Disponible',
    '1': 'Alquilado'
};

document.querySelectorAll('.coches-index .grid-view .table tbody tr td:nth-child(7)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        td.textContent = cochesAlquilados[valor] || '-';
    }
);

// Cambiar los nulos (no definido) en la tabla coches y mantenimientos
const cambiarValor = {
    '(no definido)': '-'
};

document.querySelectorAll('.coches-index .grid-view .table tbody tr td:nth-child(6)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        if(td.textContent === '(no definido)' || td.textContent === ''){
            td.textContent = cambiarValor[valor] || '-';
        }
    }
);

document.querySelectorAll('.mantenimientos-index .grid-view .table tbody tr td:nth-child(2)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        if(td.textContent === '(no definido)' || td.textContent === ''){
            td.textContent = cambiarValor[valor] || '-';
        }
    }
);

// Eliminar espacios en la descripción del mantenimiento
$(document).ready(function(){
    $('#descripcion').blur(function(){
        var valor = $(this).val().trim();
        $(this).val(valor);
    });
});

// Eliminar los espacios en el nombre del empleado
$(document).ready(function(){
    $('#tu_input_id_aqui').blur(function(){
        var valor = $(this).val().trim();
        $(this).val(valor);
    });
});

// Reemplazar coma por punto
function replaceCommaWithDot(input) {
    input.value = input.value.replace(',', '.');
}

// Calcular precio contrato
document.addEventListener("DOMContentLoaded", function () {
  calcularPrecio();
});

function calcularPrecio() {
    var selectElement = document.getElementById("select-matricula");
    var precioTotal = document.querySelector(".field-precio-total #precio-total");
    var fechaInicio = document.querySelector("#alquilan-fecha_inicio").value;
    var hiddenMatriculaField = document.getElementById("hidden-matricula-field");
    var precio;
  
    var splitFechaInicio = fechaInicio.split("-");
    var fechaFormateadaInicio = splitFechaInicio[1] + "/" + splitFechaInicio[0] + "/" + splitFechaInicio[2];
  
    var fechaInicioAlquiler = new Date(fechaFormateadaInicio);
    
    // Crear un campo oculto para la matrícula
    var hiddenMatriculaField = document.createElement("input");
    hiddenMatriculaField.type = "hidden";
    hiddenMatriculaField.name = "Alquilan[matricula]";
    document.querySelector("form").appendChild(hiddenMatriculaField);
  
    selectElement.addEventListener("change", function () {
        var selectedOption = selectElement.options[selectElement.selectedIndex];
        precio = selectedOption.value;
        var optionText = selectedOption.textContent; 
        var matriculaMarcaModelo = optionText.split(' - '); 
        var matricula = matriculaMarcaModelo[0];
        var marcaModelo = matriculaMarcaModelo[1];
        var marcaModeloArray = marcaModelo.split(' '); 
        var marca = marcaModeloArray[0];
        var modelo = marcaModeloArray.slice(1).join(' ');
        
        console.log("Matrícula: " + matricula);
        console.log("Marca: " + marca);
        console.log("Modelo: " + modelo);
        //precioTotal.value = precio;
        
        hiddenMatriculaField.value = matricula;
      
    });
    
    $("#alquilan-fecha_fin").datepicker({
      minDate: new Date(), // Establece la fecha mínima como hoy
      maxDate: "+3m", // Establece la fecha máxima como tres meses desde hoy
      dateFormat: "dd-mm-yy", // Formato de fecha
      beforeShowDay: function (date) {
        var day = date.getDay();
        // Devuelve [false] si el día es domingo, de lo contrario [true]
        return [day != 0];
      },
      onSelect: function (selectedDate) {
        var fechaFinAlquiler = $("#alquilan-fecha_fin").val();

        // Convertir el formato de la fecha
        var splitFechaFin = fechaFinAlquiler.split("-");
        var fechaFormateadaFin = splitFechaFin[1] + "-" + splitFechaFin[0] + "-" + splitFechaFin[2];
  
        var fechaFinAlquiler = new Date(fechaFormateadaFin);
        console.log(fechaFormateadaFin);
        console.log("Fecha de fin de alquiler (convertida):", fechaFinAlquiler); 
        console.log("Fecha de inicio de alquiler:", fechaInicioAlquiler); 
  
        // Calcular la diferencia en milisegundos
        var diferenciaMilisegundos = fechaFinAlquiler - fechaInicioAlquiler;
  
        // Convertir la diferencia de milisegundos a días
        var diferenciaDias = diferenciaMilisegundos / (1000 * 60 * 60 * 24);
  
        console.log("Diferencia en días:", diferenciaDias + 1);
        
        var precioFinal = precio * (diferenciaDias + 1);
  
        precioTotal.value = precioFinal;
      }
    });
  }
  
// Calcular precio presupuesto
document.addEventListener("DOMContentLoaded", function () {
  calcularPrecioPresupuesto();
});

function calcularPrecioPresupuesto() {
    var selectElement = document.getElementById("select-matricula-presupuesto");
    var precioTotal = document.querySelector(".field-precio-total #precio-total");
    var fechaInicio = document.querySelector("#presupuesto-fecha_inicio").value;
    var hiddenMatriculaField = document.getElementById("hidden-matricula-field-presupuesto");
    var precio;
  
    var splitFechaInicio = fechaInicio.split("-");
    var fechaFormateadaInicio = splitFechaInicio[1] + "/" + splitFechaInicio[0] + "/" + splitFechaInicio[2];
  
    var fechaInicioAlquiler = new Date(fechaFormateadaInicio);
    
    // Crear un campo oculto para la matrícula
    var hiddenMatriculaField = document.createElement("input");
    hiddenMatriculaField.type = "hidden";
    hiddenMatriculaField.name = "Presupuesto[matricula]";
    document.querySelector("form").appendChild(hiddenMatriculaField);
  
    selectElement.addEventListener("change", function () {
        var selectedOption = selectElement.options[selectElement.selectedIndex];
        precio = selectedOption.value;
        var optionText = selectedOption.textContent; 
        var matriculaMarcaModelo = optionText.split(' - '); 
        var matricula = matriculaMarcaModelo[0];
        var marcaModelo = matriculaMarcaModelo[1];
        var marcaModeloArray = marcaModelo.split(' '); 
        var marca = marcaModeloArray[0];
        var modelo = marcaModeloArray.slice(1).join(' ');
        
        console.log("Matrícula: " + matricula);
        console.log("Marca: " + marca);
        console.log("Modelo: " + modelo);
        //precioTotal.value = precio;
        
        hiddenMatriculaField.value = matricula;
      
    });
    
    $("#presupuesto-fecha_fin").datepicker({
      minDate: new Date(), // Establece la fecha mínima como hoy
      maxDate: "+3m", // Establece la fecha máxima como tres meses desde hoy
      dateFormat: "dd-mm-yy", // Formato de fecha
      beforeShowDay: function (date) {
        var day = date.getDay();
        // Devuelve [false] si el día es domingo, de lo contrario [true]
        return [day != 0];
      },
      onSelect: function (selectedDate) {
        var fechaFinAlquiler = $("#presupuesto-fecha_fin").val();

        // Convertir la fecha al formato "m-d-Y" (mes-día-año)
        var splitFechaFin = fechaFinAlquiler.split("-");
        var fechaFormateadaFin = splitFechaFin[1] + "-" + splitFechaFin[0] + "-" + splitFechaFin[2];
  
        var fechaFinAlquiler = new Date(fechaFormateadaFin);
        console.log(fechaFormateadaFin);
        console.log("Fecha de fin de alquiler (convertida):", fechaFinAlquiler); 
        console.log("Fecha de inicio de alquiler:", fechaInicioAlquiler); 
  
        // Calcular la diferencia en milisegundos
        var diferenciaMilisegundos = fechaFinAlquiler - fechaInicioAlquiler;
  
        // Convertir la diferencia de milisegundos a días
        var diferenciaDias = diferenciaMilisegundos / (1000 * 60 * 60 * 24);
  
        console.log("Diferencia en días:", diferenciaDias + 1);
        
        var precioFinal = precio * (diferenciaDias + 1);
  
        precioTotal.value = precioFinal;
      }
    });
  }

// PlaceHolder de fechas
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('#presupuesto-fecha_fin');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Seleccione una fecha');
        inputElement.style.display = 'block'; 
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('#mantenimientos-fecha');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Seleccione una fecha');
        inputElement.style.display = 'block'; 
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('#alquilan-fecha_fin');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Seleccione una fecha');
        inputElement.style.display = 'block'; 
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('#clientes-fecha_permiso_conducir');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Seleccione una fecha');
        inputElement.style.display = 'block'; 
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('#atienden-fecha');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Seleccione una fecha');
        inputElement.style.display = 'block'; 
    }
});

//Regular cada cuanto cambian las imágens del slider
//$('.carousel').carousel({
//  interval: 4000
//});
